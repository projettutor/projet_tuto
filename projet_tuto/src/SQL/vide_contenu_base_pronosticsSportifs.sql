-- ============================================================================
-- Fichier : vide_contenu_base_pronosticsSportifs
-- Auteur  : Jamal Malki
-- Date    : Mai 2014
-- Role    : Creation des dépendances de références sur la base de donnees 
--			  "Pronostics-Sportifs" 
-- ============================================================================
delete from PRONOSTIC ;
delete from PRONOSTIQEUR ;
delete from MATCH ;
delete from EQUIPE ;
delete from POULE ;
delete from TOURNOI ;
delete from CONFIG_TOURNOI ;
