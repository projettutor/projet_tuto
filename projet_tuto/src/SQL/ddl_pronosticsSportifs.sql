-- ============================================================================
-- Fichier : ddl_pronosticsSportifs
-- Auteur  : Jamal Malki
-- Date    : Mai 2014
-- Role    : Creation des tables de la base de donnees "Pronostics-Sportifs" 
-- ============================================================================

 --Suppression des tables 
DROP TABLE CONFIG_TOURNOI CASCADE CONSTRAINT;
DROP TABLE TOURNOI CASCADE CONSTRAINT;
DROP TABLE POULE CASCADE CONSTRAINT;
DROP TABLE EQUIPE CASCADE CONSTRAINT;
DROP TABLE PRONOSTIQEUR CASCADE CONSTRAINT;
DROP TABLE MATCH CASCADE CONSTRAINT;
DROP TABLE PRONOSTIC CASCADE CONSTRAINT;

-- ============================================================================
-- Creation de la Table CONFIG_TOURNOI                               
-- ============================================================================
 create table CONFIG_TOURNOI (
idConfig			NUMBER				NOT NULL,
pronosticOK			NUMBER				NOT NULL,
pronosticKO			NUMBER				NOT NULL,
pasPronostic                    NUMBER				NOT NULL,
constraint pk_CONFIG_TOURNOI primary key (idConfig)
);

-- ============================================================================
-- Creation de la Table TOURNOI                               
-- ============================================================================
 create table TOURNOI (
idTournoi			NUMBER				NOT NULL,
nomTournoi			VARCHAR2(20)                    NOT NULL, 
dateDebut			DATE				NOT NULL,
dateFin				DATE				NOT NULL,
config				NUMBER				NOT NULL,
constraint pk_TOURNOI primary key (idTournoi),
constraint uk_TOURNOI unique (nomTournoi, dateDebut)
);

-- ============================================================================
-- Creation de la Table POULE                               
-- ============================================================================
 create table POULE (
idPoule			NUMBER						NOT NULL,
nomEPoule		VARCHAR2(20)                                    NOT NULL, 
nbMaxEquipe		NUMBER						NOT NULL,
tournoi			NUMBER						NOT NULL,
constraint pk_POULE primary key (idPoule)
);

-- ============================================================================
-- Creation de la Table EQUIPE                               
-- ============================================================================
 create table EQUIPE (
idEquipe		NUMBER						NOT NULL,
nomEquipe		VARCHAR2(20)                                    NOT NULL, 
pays			VARCHAR2(50)                                    NOT NULL,
poule			NUMBER						NOT NULL,
constraint pk_EQUIPE primary key (idEquipe),
constraint uk_EQUIPE unique (nomEquipe)
);

-- ============================================================================
-- Creation de la Table PRONOSTIQEUR                               
-- ============================================================================
 create table PRONOSTIQEUR (
idPro			NUMBER						NOT NULL,
nomPrenom		VARCHAR2(100)                                   NOT NULL, 
login			VARCHAR2(20)                                    NOT NULL, 
mdp				VARCHAR2(20)				NOT NULL, 
nbPoints		NUMBER	default 0                               NOT NULL,
classement		NUMBER	default 0                               NOT NULL,
constraint pk_PRONOSTIQEUR primary key (idPro),
constraint uk_PRONOSTIQEUR unique (login)
);

-- ============================================================================
-- Creation de la Table MATCH                               
-- ============================================================================
 create table MATCH (
idMatch			NUMBER						NOT NULL,
dateDebut		DATE default sysdate                            NOT NULL,
equipeA			NUMBER						NOT NULL,
equipeB			NUMBER						NOT NULL,
dateFin			DATE 						NULL,
scoreEqA		NUMBER						NULL,
scoreEqB		NUMBER						NULL,
resultatEqA		CHAR(1)						NULL,
resultatEqB		CHAR(1)						NULL,
constraint pk_MATCH primary key (idMatch),
constraint uk_MATCH unique (dateDebut, equipeA, equipeB),
constraint ck_match_resultatEqA check (upper(resultatEqA) in ('G', 'N', 'P')),
constraint ck_match_resultatEqB check (upper(resultatEqB) in ('G', 'N', 'P')),
constraint ck_match_equipeAG check (upper(resultatEqA) <> 'G' 
									OR  upper(resultatEqB) = 'P'),
constraint ck_match_equipeBG check (upper(resultatEqB) <> 'G' 
									OR  upper(resultatEqA) = 'P'),
constraint ck_match_equipeAP check (upper(resultatEqA) <> 'P' 
									OR  upper(resultatEqB) = 'G'),
constraint ck_match_equipeBP check (upper(resultatEqB) <> 'P' 
									OR  upper(resultatEqA) = 'G'),
constraint ck_match_equipeABN check (upper(resultatEqA) <> 'N' OR  upper(resultatEqB) = 'N')
);

-- ============================================================================
-- Creation de la Table PRONOSTIC                               
-- ============================================================================
 create table PRONOSTIC (
pronostiqueur                           NUMBER                                          NOT NULL,
match					NUMBER						NOT NULL,
datePronostic                           DATE default sysdate                            NOT NULL,
scoreEqA				NUMBER						NULL,
scoreEqB				NUMBER						NULL,
resultatEqA				CHAR(1)						NOT NULL,
resultatEqB				CHAR(1)						NOT NULL,
constraint pk_PRONOSTIC primary key (pronostiqueur, match),
constraint ck_pro_resultatEqA check (upper(resultatEqA) in ('G', 'N', 'P')),
constraint ck_pro_resultatEqB check (upper(resultatEqB) in ('G', 'N', 'P'))
);

