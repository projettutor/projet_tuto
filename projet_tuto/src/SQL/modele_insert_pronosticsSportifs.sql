-- ============================================================================
 -- Fichier : modele_insert_pronosticsSportifs
 -- Auteur  : Jamal Malki
 -- Date    : Mai 2014
 -- Role    : Modele d'insertion dans les tables de la base de donnees 
--			 "Pronostics-Sportifs"
-- ============================================================================

alter session set NLS_DATE_FORMAT='ss:mi:hh24 dd/mm/yyyy'; 

-- Un trounoi
-- Remarque : le modele d'insert utilise le même tournoi 
insert into CONFIG_TOURNOI (IDCONFIG,PRONOSTICOK,PRONOSTICKO,PASPRONOSTIC)   
values (1, 2, 0, -1);
insert into TOURNOI (IDTOURNOI,NOMTOURNOI,DATEDEBUT,DATEFIN,CONFIG)
values (1, 'tournoi1', sysdate, sysdate+30, 1);

-- Un poule
insert into POULE (IDPOULE,NOMEPOULE,NBMAXEQUIPE,TOURNOI)
values (10, 'poule10', 2, 1);

-- Une équipe 
insert into POULE (IDPOULE,NOMEPOULE,NBMAXEQUIPE,TOURNOI)
values (11, 'poule11', 2, 1);
insert into EQUIPE (IDEQUIPE,NOMEQUIPE,PAYS,POULE)   
values (20, 'equipe20', 'france', 11);

-- Un pronostiqueur 
insert into PRONOSTIQEUR (IDPRO,NOMPRENOM,LOGIN,MDP) 
values (30, 'pronostiqueur30', 'pro30', 'pro30');

-- Un match 
insert into POULE (IDPOULE,NOMEPOULE,NBMAXEQUIPE,TOURNOI)
values (12, 'poule12', 2, 1);
insert into EQUIPE (IDEQUIPE,NOMEQUIPE,PAYS,POULE)   
values (21, 'equipe21', 'Espagne', 12);
insert into EQUIPE (IDEQUIPE,NOMEQUIPE,PAYS,POULE)   
values (22, 'equipe22', 'Portugal', 12);
insert into MATCH (IDMATCH,DATEDEBUT,EQUIPEA,EQUIPEB)
values (2122, sysdate, 21, 22); 

-- Un pronostic
-- Utilise le match 2122 et le pronostiqueur 30
insert into PRONOSTIC (PRONOSTIQUEUR,MATCH,DATEPRONOSTIC,SCOREEQA,SCOREEQB,
					   RESULTATEQA,RESULTATEQB)
values (30, 2122, sysdate, 1, 0, 'G', 'P'); 

