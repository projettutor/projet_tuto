/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gestionPronos.modelPres;

import gestionPronos.modele.Pronostic;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Antoine
 */
public class modelPresTabProno extends AbstractTableModel
{
    private List<Pronostic> listProno;
    private String[] listNomColonne;
    
    public modelPresTabProno(List<Pronostic> pronos)
    {
        this.listProno = pronos;
        this.listNomColonne = new String[]{"Date", "Match", "Résultat du match", "Prono Equipe1", "Prono Equipe2", "Resultat du Prono"};
    }
    
    @Override
    public int getRowCount() {
        return this.listProno.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }
        

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex)
        {
            case 0: return this.listProno.get(rowIndex).getDatePronostic();
            case 1: return this.listProno.get(rowIndex).getMatch();
            case 2: return this.listProno.get(rowIndex).getMatch().getScoreEqA() + " - " + this.listProno.get(rowIndex).getMatch().getScoreEqB();
            case 3: return this.listProno.get(rowIndex).getResultatEqA();
            case 4: return this.listProno.get(rowIndex).getResultatEqB();
            case 5: return this.listProno.get(rowIndex).getPronoBon();
        }
        return null;
    }
    
        @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        switch(columnIndex)
        {
            case 0 : return java.lang.String.class;
            case 1 : return gestionPronos.modele.Match.class;
            case 2 : return java.lang.String.class;
            case 3 : return gestionPronos.modele.TypeResultatEquipeMatch.class;
            case 4 : return gestionPronos.modele.TypeResultatEquipeMatch.class;
            case 5 : return java.lang.String.class;
        }
        return null;
    }
    
    @Override
    public String getColumnName(int columnIndex)
    {
        return this.listNomColonne[columnIndex];
    }
    
}
