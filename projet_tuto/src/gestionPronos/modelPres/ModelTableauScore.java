/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gestionPronos.modelPres;

import gestionPronos.modele.Pronostiqueur;
import gestionPronos.modele.Resultat;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Quentin M
 */
public class ModelTableauScore extends AbstractTableModel{
   private List<Resultat> listepronos; 
    private String[] listNomColonne;

    public ModelTableauScore(List<Resultat> listepronos) {
        this.listepronos = listepronos;
        this.listNomColonne = new String[]{"Classement" ,"Pronostiqueur","Score"};
    }

  
    
    

    @Override
    public int getRowCount() {
        return this.listepronos.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int i, int icol) {
        switch(icol)
        {
            case 0: return this.listepronos.get(i).getClassement();
            case 1: return this.listepronos.get(i).getPronostiqueur().getNomPrenom();
            case 2: return this.listepronos.get(i).getNbPoints();
            
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int i) {
        switch(i)
        {
            case 0 : return  java.lang.Integer.class;
            case 1 : return  java.lang.String.class;    
            case 2 : return java.lang.Integer.class;
           
        }
        return null;
    }

    @Override
    public String getColumnName(int column) {
        return this.listNomColonne[column]; //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
