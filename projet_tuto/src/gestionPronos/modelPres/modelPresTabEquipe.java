/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gestionPronos.modelPres;

import gestionPronos.modele.Equipe;
import gestionPronos.modele.Pronostic;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Quentin M
 */
public class modelPresTabEquipe extends AbstractTableModel
{
    private List<Equipe> listequipes;  
    private Object[][] data;
    private String[] listNomColonne;               
    
    public modelPresTabEquipe(List<Equipe> equipes)
            
    {
        this.listequipes=equipes;
        this.listNomColonne = new String[]{"Nom ", "Pays", "Selectionner"};        
        this.data = new Object[this.listequipes.size()][3];
        for(int i = 0; i < this.listequipes.size(); i++)
        {
            this.data[i][2] = false;
        }
    }

    public List<Equipe> getListequipes() {
        return listequipes;
    }
    
    public ArrayList<Equipe> getEquipesSelectionnee()
    {
        ArrayList<Equipe> tabRetour = new ArrayList<>();
        for(int i = 0; i < this.listequipes.size(); i++)
        {
            if(((Boolean)(this.data[i][2])).booleanValue() == true)
            {
                tabRetour.add(this.listequipes.get(i));
            }
        }
        return tabRetour;
    }
        
    @Override
    public int getRowCount() {
        return this.listequipes.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }
        

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex)
        {
            case 0: return this.listequipes.get(rowIndex).getNomEquipe();
            case 1: return this.listequipes.get(rowIndex).getPays();
            case 2: return (Boolean)(this.data[rowIndex])[2];
        }
        return null;
    }
    
        @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        switch(columnIndex)
        {
            case 0 : return java.lang.String.class;
            case 1 : return java.lang.String.class;
            case 2 : return java.lang.Boolean.class;
        }
        return null;
    }
     @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex == 2); // Editable si l'index de colonne correspond à la dernière. (Normalement l'index commence à 0 mais dans le cas contraire c'est dans le -1 qu'il faut faire).
    }

      
    
    @Override
    public String getColumnName(int columnIndex)
    {
        return this.listNomColonne[columnIndex];
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    //ICI pour les checkbox
        if(aValue instanceof Boolean)
            this.data[rowIndex][columnIndex] = ((Boolean)aValue);
            
    }    
}
