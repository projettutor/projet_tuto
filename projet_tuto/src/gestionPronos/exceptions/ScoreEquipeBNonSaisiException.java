/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionPronos.exceptions;

import javax.swing.JOptionPane;

/**
 * Cette exception concerne le CU SaisirResultatMatch
 * @author Nicolas
 */
public class ScoreEquipeBNonSaisiException extends Exception {

    /**
     * Creates a new instance of
     * <code>ScoreEquipeBNonSaisiException</code> without detail message.
     */
    public ScoreEquipeBNonSaisiException() {
    }

    /**
     * Constructs an instance of
     * <code>ScoreEquipeBNonSaisiException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public ScoreEquipeBNonSaisiException(String msg) {
        JOptionPane.showMessageDialog(null, 
                                       msg,
                                       "Score équipe B non saisi", 
                                       JOptionPane.ERROR_MESSAGE);
    }
}
