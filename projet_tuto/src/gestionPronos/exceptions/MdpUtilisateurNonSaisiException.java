/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionPronos.exceptions;

import javax.swing.JOptionPane;

/**
 * Cette exception concerne le CU Declarer
 * @author Nicolas
 */
public class MdpUtilisateurNonSaisiException extends Exception {

    /**
     * Creates a new instance of
     * <code>MdpUtilisateurNonSaisiException</code> without detail message.
     */
    public MdpUtilisateurNonSaisiException() {
    }

    /**
     * Constructs an instance of
     * <code>MdpUtilisateurNonSaisiException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public MdpUtilisateurNonSaisiException(String msg) {
        JOptionPane.showMessageDialog(null, 
                                       msg,
                                       "Mot de passe non saisi", 
                                       JOptionPane.ERROR_MESSAGE);
    }
}
