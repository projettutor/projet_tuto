/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionPronos.exceptions;

import javax.swing.JOptionPane;

/**
 * Cette exception concerne le CU DeclarerPouleEquipe
 * @author Nicolas
 */
public class NomEquipeManquantException extends Exception {

    /**
     * Creates a new instance of
     * <code>NomEquipeManquantException</code> without detail message.
     */
    public NomEquipeManquantException() {
    }

    /**
     * Constructs an instance of
     * <code>NomEquipeManquantException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public NomEquipeManquantException(String msg) {
        JOptionPane.showMessageDialog(null, 
                                       msg,
                                       "Nom d'équipe manquant", 
                                       JOptionPane.ERROR_MESSAGE);
    }
}
