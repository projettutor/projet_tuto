/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionPronos.exceptions;

import javax.swing.JOptionPane;

/**
 * Cette exception concerne le CU Declarer
 * @author Nicolas
 */
public class UtilisateurExisteDejaException extends Exception {

    /**
     * Creates a new instance of
     * <code>UtilisateurExisteDejaException</code> without detail message.
     */
    public UtilisateurExisteDejaException() {
    }

    /**
     * Constructs an instance of
     * <code>UtilisateurExisteDejaException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public UtilisateurExisteDejaException(String msg) {
        JOptionPane.showMessageDialog(null, 
                                       msg,
                                       "Cet utilisateur existe déjà", 
                                       JOptionPane.ERROR_MESSAGE);
    }
}
