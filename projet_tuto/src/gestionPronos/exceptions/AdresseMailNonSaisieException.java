/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionPronos.exceptions;

import javax.swing.JOptionPane;

/**
 * Cette exception concerne le CU Declarer
 * @author Nicolas
 */
public class AdresseMailNonSaisieException extends Exception {
    /**
     * Creates a new instance of
     * <code>AdresseMailNonSaisieException</code> without detail message.
     */
    public AdresseMailNonSaisieException() {
    }

    /**
     * Constructs an instance of
     * <code>AdresseMailNonSaisieException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public AdresseMailNonSaisieException(String msg) {
        JOptionPane.showMessageDialog(null, 
                                       msg,
                                       "Adresse mail non saisie", 
                                       JOptionPane.ERROR_MESSAGE);
    }
}
