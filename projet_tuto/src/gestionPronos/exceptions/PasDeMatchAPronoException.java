/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gestionPronos.exceptions;

import javax.swing.JOptionPane;

/**
 *
 * @author Antoine
 */
public class PasDeMatchAPronoException extends Exception {

    /**
     * Creates a new instance of <code>PasDeMatchAPronoException</code> without
     * detail message.
     */
    public PasDeMatchAPronoException() {
    }

    /**
     * Constructs an instance of <code>PasDeMatchAPronoException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public PasDeMatchAPronoException(String msg) {
        JOptionPane.showMessageDialog(null, msg, "Pas de match a pronostiquer", JOptionPane.ERROR_MESSAGE);
    }
}
