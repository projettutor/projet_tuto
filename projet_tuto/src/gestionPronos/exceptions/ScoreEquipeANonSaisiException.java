/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionPronos.exceptions;

import javax.swing.JOptionPane;

/**
 * Cette exception concerne le CU SaisirResultatMatch
 * @author Nicolas
 */
public class ScoreEquipeANonSaisiException extends Exception {

    /**
     * Creates a new instance of
     * <code>ScoreEquipeANonSaisiException</code> without detail message.
     */
    public ScoreEquipeANonSaisiException() {
    }

    /**
     * Constructs an instance of
     * <code>ScoreEquipeANonSaisiException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public ScoreEquipeANonSaisiException(String msg) {
        JOptionPane.showMessageDialog(null, 
                                       msg,
                                       "Score équipe A non saisi", 
                                       JOptionPane.ERROR_MESSAGE);
    }
}
