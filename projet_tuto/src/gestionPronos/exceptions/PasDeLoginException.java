/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionPronos.exceptions;

import javax.swing.JOptionPane;

/**
 * Cette exception concerne le CU Declarer
 * @author Nicolas
 */
public class PasDeLoginException extends Exception {

    /**
     * Creates a new instance of
     * <code>PasDeLoginException</code> without detail message.
     */
    public PasDeLoginException() {
    }

    /**
     * Constructs an instance of
     * <code>PasDeLoginException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public PasDeLoginException(String msg) {
        JOptionPane.showMessageDialog(null, 
                                       msg,
                                       "Pas de login saisi", 
                                       JOptionPane.ERROR_MESSAGE);
    }
}
