/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionPronos.exceptions;

import javax.swing.JOptionPane;

/** 
 * Cette exception concerne le CU Declarer
 * @author Nicolas
 */
public class NomUtilisateurNonSaisiException extends Exception {

    /**
     * Creates a new instance of
     * <code>NomUtilisateurNonSaisiException</code> without detail message.
     */
    public NomUtilisateurNonSaisiException() {
    }

    /**
     * Constructs an instance of
     * <code>NomUtilisateurNonSaisiException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public NomUtilisateurNonSaisiException(String msg) {
        JOptionPane.showMessageDialog(null, 
                                       msg,
                                       "Nom d'utilisateur non saisi", 
                                       JOptionPane.ERROR_MESSAGE);
    }
}
