/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionPronos.exceptions;

import javax.swing.JOptionPane;

/**
 * Cette exception concerne le CU Declarer
 * @author Nicolas
 */
public class NomPrenomNonSaisiException extends Exception {

    /**
     * Creates a new instance of
     * <code>NomPrenomNonSaisiException</code> without detail message.
     */
    public NomPrenomNonSaisiException() {
    }

    /**
     * Constructs an instance of
     * <code>NomPrenomNonSaisiException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public NomPrenomNonSaisiException(String msg) {
        JOptionPane.showMessageDialog(null, 
                                       msg,
                                       "Nom/prénom non saisi", 
                                       JOptionPane.ERROR_MESSAGE);
    }
}
