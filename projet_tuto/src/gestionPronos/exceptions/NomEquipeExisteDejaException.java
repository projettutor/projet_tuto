/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionPronos.exceptions;

import javax.swing.JOptionPane;

/**
 * Cette exception concerne le CU DeclarerPouleEquipe
 * @author Nicolas
 */
public class NomEquipeExisteDejaException extends Exception {
    
    /**
     * Creates a new instance of
     * <code>NomEquipeExisteDejaException</code> without detail message.
     */
    public NomEquipeExisteDejaException() {
    }

    /**
     * Constructs an instance of
     * <code>NomEquipeExisteDejaException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public NomEquipeExisteDejaException(String msg) {
        JOptionPane.showMessageDialog(null, 
                                       msg,
                                       "Nom d'équipe déjà existant", 
                                       JOptionPane.ERROR_MESSAGE);
    }
}
