/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionPronos.exceptions;

import javax.swing.JOptionPane;

/**
 * Cette exception concerne le CU Declarer
 * @author Nicolas
 */
public class PasDeMdpException extends Exception {

    /**
     * Creates a new instance of
     * <code>PasDeMdpException</code> without detail message.
     */
    public PasDeMdpException() {
    }

    /**
     * Constructs an instance of
     * <code>PasDeMdpException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public PasDeMdpException(String msg) {
        JOptionPane.showMessageDialog(null, 
                                       msg,
                                       "Mot de passe non saisi", 
                                       JOptionPane.ERROR_MESSAGE);
    }
}
