/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionPronos.exceptions;

import javax.swing.JOptionPane;

/**
 * Cette exception concerne le CU DeclarerPouleEquipe
 * @author Nicolas
 */
public class PaysEquipeManquantException extends Exception {

    /**
     * Creates a new instance of
     * <code>PaysEquipeManquantException</code> without detail message.
     */
    public PaysEquipeManquantException() {
    }

    /**
     * Constructs an instance of
     * <code>PaysEquipeManquantException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public PaysEquipeManquantException(String msg) {
        JOptionPane.showMessageDialog(null, 
                                       msg,
                                       "Pays manquant", 
                                       JOptionPane.ERROR_MESSAGE);
    }
}
