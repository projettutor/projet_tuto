/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionPronos.exceptions;

import javax.swing.JOptionPane;

/**
 * Cette exception concerne le CU SaisirPronostic
 * @author Nicolas
 */
public class PasDePronosticSaisiException extends Exception {

    /**
     * Creates a new instance of
     * <code>PasDePronosticSaisiException</code> without detail message.
     */
    public PasDePronosticSaisiException() {
    }

    /**
     * Constructs an instance of
     * <code>PasDePronosticSaisiException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public PasDePronosticSaisiException(String msg) {
        JOptionPane.showMessageDialog(null, 
                                       msg,
                                       "Pronostic non saisi", 
                                       JOptionPane.ERROR_MESSAGE);
    }
}
