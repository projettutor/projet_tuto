/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionPronos.exceptions;

import javax.swing.JOptionPane;

/**
 * Cette exception concerne le CU SaisirPronostic
 * @author Nicolas
 */
public class PronosticDejaSaisiException extends Exception {

    /**
     * Creates a new instance of
     * <code>PronosticDejaSaisiException</code> without detail message.
     */
    public PronosticDejaSaisiException() {
    }

    /**
     * Constructs an instance of
     * <code>PronosticDejaSaisiException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public PronosticDejaSaisiException(String msg) {
        JOptionPane.showMessageDialog(null, 
                                       msg,
                                       "Pronostic déjà saisi", 
                                       JOptionPane.ERROR_MESSAGE);
    }
}
