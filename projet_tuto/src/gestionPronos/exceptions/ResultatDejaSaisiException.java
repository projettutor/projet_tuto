/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionPronos.exceptions;

import javax.swing.JOptionPane;

/**
 * Cette exception concerne le CU SaisirResultatMatch
 * @author Nicolas
 */
public class ResultatDejaSaisiException extends Exception {

    /**
     * Creates a new instance of
     * <code>ResultatDejaSaisiException</code> without detail message.
     */
    public ResultatDejaSaisiException() {
    }

    /**
     * Constructs an instance of
     * <code>ResultatDejaSaisiException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public ResultatDejaSaisiException(String msg) {
        JOptionPane.showMessageDialog(null, 
                                       msg,
                                       "Ce résultat a déjà été saisi", 
                                       JOptionPane.ERROR_MESSAGE);
    }
}
