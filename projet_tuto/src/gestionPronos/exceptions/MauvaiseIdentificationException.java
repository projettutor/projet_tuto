/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionPronos.exceptions;

import javax.swing.JOptionPane;

/**
 *
 * @author Nicolas
 */
public class MauvaiseIdentificationException extends Exception {
    
    /**
     * Creates a new instance of
     * <code>MauvaiseIdentificationException</code> without detail message.
     */
    public MauvaiseIdentificationException() {
    }

    /**
     * Constructs an instance of
     * <code>MauvaiseIdentificationException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public MauvaiseIdentificationException(String msg) {
        JOptionPane.showMessageDialog(null, 
                                       msg,
                                       "Erreur d'authentification", 
                                       JOptionPane.ERROR_MESSAGE);
    }
}
