/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gestionPronos.modele;

/**
 *
 * @author Antoine
 */
public class Resultat {
    private int classement;
    private int nbPoints;
    private Pronostiqueur pronostiqueur;

    public Resultat(int classement, int nbPoints, Pronostiqueur pronostiqueur) {
        this.classement = classement;
        this.nbPoints = nbPoints;
        this.pronostiqueur = pronostiqueur;
    }

    public int getClassement() {
        return classement;
    }

    public int getNbPoints() {
        return nbPoints;
    }

    public Pronostiqueur getPronostiqueur() {
        return pronostiqueur;
    }

    public void setClassement(int classement) {
        this.classement = classement;
    }

    public void setNbPoints(int nbPoints) {
        this.nbPoints = nbPoints;
    }

    public void setPronostiqueur(Pronostiqueur pronostiqueur) {
        this.pronostiqueur = pronostiqueur;
    }
    
    
}
