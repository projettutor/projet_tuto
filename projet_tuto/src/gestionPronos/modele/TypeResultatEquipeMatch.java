/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gestionPronos.modele;

/**
 *
 * @author Antoine
 */
public enum TypeResultatEquipeMatch {
    VICTOIRE('g'),
    NUL('n'),
    DEFAITE('p');
    private final char caracCorres;
    private TypeResultatEquipeMatch(char caractereCorresp)
    {
        this.caracCorres = caractereCorresp;
    }
    public char getCaractere()
    {
        return this.caracCorres;
    }
    public static TypeResultatEquipeMatch getCaracAssoc(char carac)
    {
        for(TypeResultatEquipeMatch tresu : TypeResultatEquipeMatch.values())
        {
            if(tresu.getCaractere() == carac)
                return tresu;
        }
        return null;
    }
    

}
