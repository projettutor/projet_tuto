/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gestionPronos.modele;

import java.util.Calendar;

/**
 *
 * @author Antoine
 */
public class Pronostic {
    private String datePronostic;
    private TypeResultatEquipeMatch resultatEqA;
    private TypeResultatEquipeMatch resultatEqB;
    private Pronostiqueur pronostiqueur;
    private Match match;
    private boolean pronoBon;

    public Pronostic(String datePronostic, TypeResultatEquipeMatch resultatEqA, TypeResultatEquipeMatch resultatEqB, Pronostiqueur pronostiqueur, Match match) {
        this.datePronostic = datePronostic;
        this.resultatEqA = resultatEqA;
        this.resultatEqB = resultatEqB;
        this.pronostiqueur = pronostiqueur;
        this.match = match;
    }
        public Pronostic(char char1, char char2, Pronostiqueur pronostiqueur, Match match, String datePronostic) {
        this.resultatEqA = TypeResultatEquipeMatch.getCaracAssoc(char1);
        this.resultatEqB = TypeResultatEquipeMatch.getCaracAssoc(char2);
        this.pronostiqueur = pronostiqueur;
        this.match = match;
        this.datePronostic = datePronostic;
    }

    public Pronostic(TypeResultatEquipeMatch resultatEqA, TypeResultatEquipeMatch resultatEqB, Pronostiqueur pronostiqueur, Match match) {
        this.resultatEqA = resultatEqA;
        this.resultatEqB = resultatEqB;
        this.pronostiqueur = pronostiqueur;
        this.match = match;
    }

    


    
    
    
    
    //Getters et Setters
    public String getPronoBon(){
        if(this.match.getDateFin() != null)
        {
            if(this.match.getResultatEqA().getCaractere() == this.resultatEqA.getCaractere() && (this.match.getResultatEqB().getCaractere() == this.resultatEqB.getCaractere()))
                return "PRONO BON";
            else
                return "PRONO MAUVAIS";
        }
        else
            return "PAS JOUE";
    }
    
    public Pronostiqueur getPronostiqueur() {
        return pronostiqueur;
    }

    public void setPronostiqueur(Pronostiqueur pronostiqueur) {
        this.pronostiqueur = pronostiqueur;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }
    
    public String getDatePronostic() {
        return datePronostic;
    }

    public void setDatePronostic(String datePronostic) {
        this.datePronostic = datePronostic;
    }


    public TypeResultatEquipeMatch getResultatEqA() {
        return resultatEqA;
    }

    public void setResultatEqA(TypeResultatEquipeMatch resultatEqA) {
        this.resultatEqA = resultatEqA;
    }

    public TypeResultatEquipeMatch getResultatEqB() {
        return resultatEqB;
    }

    public void setResultatEqB(TypeResultatEquipeMatch resultatEqB) {
        this.resultatEqB = resultatEqB;
    }
    
    
    
}
