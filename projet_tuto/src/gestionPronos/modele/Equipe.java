/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gestionPronos.modele;

import java.util.Objects;

/**
 *
 * @author Antoine
 */
public class Equipe {
    private int idEquipe;
    private String nomEquipe;
    private String pays;

    public Equipe(int idEquipe, String nomEquipe, String pays) {
        this.idEquipe = idEquipe;
        this.nomEquipe = nomEquipe;
        this.pays = pays;
    }

    public Equipe(String nomEquipe, String pays) {
        this.nomEquipe = nomEquipe;
        this.pays = pays;
    }

    public String getNomEquipe() {
        return nomEquipe;
    }

    public void setNomEquipe(String nomEquipe) {
        this.nomEquipe = nomEquipe;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + Objects.hashCode(this.nomEquipe);
        hash = 19 * hash + Objects.hashCode(this.pays);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Equipe other = (Equipe) obj;
        if (!Objects.equals(this.nomEquipe, other.nomEquipe)) {
            return false;
        }
        if (!Objects.equals(this.pays, other.pays)) {
            return false;
        }
        return true;
    }
    
    
    
}
