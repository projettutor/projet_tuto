/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gestionPronos.modele;

import java.util.Objects;

/**
 *
 * @author Antoine
 */
public class Match {
    private int idMatch;
    private String dateDebut;
    private String dateFin;
    private int scoreEqA;
    private int scoreEqB;
    private TypeResultatEquipeMatch resultatEqA;
    private TypeResultatEquipeMatch resultatEqB;
    private Equipe equipeA;
    private Equipe equipeB;
    private PhaseTournoi phase;

    public Match(int idMatch, String dateDebut, String dateFin, int scoreEqA, int scoreEqB, TypeResultatEquipeMatch resultatEqA, TypeResultatEquipeMatch resultatEqB, Equipe equipeA, Equipe equipeB, PhaseTournoi phase) {
        this.idMatch = idMatch;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.scoreEqA = scoreEqA;
        this.scoreEqB = scoreEqB;
        this.resultatEqA = resultatEqA;
        this.resultatEqB = resultatEqB;
        this.equipeA = equipeA;
        this.equipeB = equipeB;
        this.phase = phase;
    }

    public Match(int idMatch, String dateDebut, Equipe equipeA, Equipe equipeB) {
        this.idMatch = idMatch;
        this.dateDebut = dateDebut;
        this.equipeA = equipeA;
        this.equipeB = equipeB;
    }

    public Match(int idMatch, String dateDebut, int scoreEqA, int scoreEqB, Equipe equipeA, Equipe equipeB, String dateFin) {
        this.idMatch = idMatch;
        this.dateDebut = dateDebut;
        this.scoreEqA = scoreEqA;
        this.scoreEqB = scoreEqB;
        this.equipeA = equipeA;
        this.equipeB = equipeB;
        this.dateFin = dateFin;
        if(this.scoreEqA > this.scoreEqB)
        {
            this.resultatEqA = TypeResultatEquipeMatch.VICTOIRE;
            this.resultatEqB = TypeResultatEquipeMatch.DEFAITE;
        }
        else  if(this.scoreEqA == this.scoreEqB)
        {
            this.resultatEqA = TypeResultatEquipeMatch.NUL;
            this.resultatEqB = TypeResultatEquipeMatch.NUL;
        }
        else
        {
            this.resultatEqA = TypeResultatEquipeMatch.DEFAITE;
            this.resultatEqB = TypeResultatEquipeMatch.VICTOIRE;

        }
    }
    
    
    
    

    public int getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(int idMatch) {
        this.idMatch = idMatch;
    }

    public String getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(String dateDebut) {
        this.dateDebut = dateDebut;
    }

    public String getDateFin() {
        return dateFin;
    }

    public void setDateFin(String dateFin) {
        this.dateFin = dateFin;
    }

    public int getScoreEqA() {
        return scoreEqA;
    }

    public void setScoreEqA(int scoreEqA) {
        this.scoreEqA = scoreEqA;
    }

    public int getScoreEqB() {
        return scoreEqB;
    }

    public void setScoreEqB(int scoreEqB) {
        this.scoreEqB = scoreEqB;
    }

    public TypeResultatEquipeMatch getResultatEqA() {
        return resultatEqA;
    }

    public void setResultatEqA(TypeResultatEquipeMatch resultatEqA) {
        this.resultatEqA = resultatEqA;
    }

    public TypeResultatEquipeMatch getResultatEqB() {
        return resultatEqB;
    }

    public void setResultatEqB(TypeResultatEquipeMatch resultatEqB) {
        this.resultatEqB = resultatEqB;
    }

    public Equipe getEquipeA() {
        return equipeA;
    }

    public void setEquipeA(Equipe equipeA) {
        this.equipeA = equipeA;
    }

    public Equipe getEquipeB() {
        return equipeB;
    }

    public void setEquipeB(Equipe equipeB) {
        this.equipeB = equipeB;
    }

    public PhaseTournoi getPhase() {
        return phase;
    }

    public void setPhase(PhaseTournoi phase) {
        this.phase = phase;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + this.idMatch;
        hash = 53 * hash + Objects.hashCode(this.dateDebut);
        hash = 53 * hash + Objects.hashCode(this.equipeA);
        hash = 53 * hash + Objects.hashCode(this.equipeB);
        hash = 53 * hash + Objects.hashCode(this.phase);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Match other = (Match) obj;
        if (this.idMatch != other.idMatch) {
            return false;
        }
        if (!Objects.equals(this.dateDebut, other.dateDebut)) {
            return false;
        }
        if (!Objects.equals(this.equipeA, other.equipeA)) {
            return false;
        }
        if (!Objects.equals(this.equipeB, other.equipeB)) {
            return false;
        }
        if (!Objects.equals(this.phase, other.phase)) {
            return false;
        }
        return true;
    }


    
    
    public Match(Equipe equipeA, Equipe equipeB)
    {
        this.equipeA = equipeA;
        this.equipeB = equipeB;
    }
    
    
    @Override
    public String toString()
    {
        return this.equipeA.getNomEquipe() + " - " + this.equipeB.getNomEquipe();
    }

    
}
