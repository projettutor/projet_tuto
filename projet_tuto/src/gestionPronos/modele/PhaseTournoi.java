/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gestionPronos.modele;


/**
 *
 * @author Antoine
 */
public class PhaseTournoi {
    private int idPhaseTournoi;
    private String libelPhaseTournoi;
    private ConfigTournoi config;

    public PhaseTournoi(int idPhaseTournoi, String libelPhaseTournoi, ConfigTournoi config) {
        this.idPhaseTournoi = idPhaseTournoi;
        this.libelPhaseTournoi = libelPhaseTournoi;
        this.config = config;
    }

    public int getIdPhaseTournoi() {
        return idPhaseTournoi;
    }

    public void setIdPhaseTournoi(int idPhaseTournoi) {
        this.idPhaseTournoi = idPhaseTournoi;
    }

    public String getLibelPhaseTournoi() {
        return libelPhaseTournoi;
    }

    public void setLibelPhaseTournoi(String libelPhaseTournoi) {
        this.libelPhaseTournoi = libelPhaseTournoi;
    }

    public ConfigTournoi getConfig() {
        return config;
    }

    public void setConfig(ConfigTournoi config) {
        this.config = config;
    }
    
    
}
