/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gestionPronos.modele;

import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Antoine
 */
public class Tournoi {
    private int idTournoi;
    private String nomTournoi;
    private Calendar dateDebut;
    private Calendar dateFin;
    private ArrayList<PhaseTournoi> phases;
    
    public Tournoi(String nomTournoi, Calendar dateDebut, Calendar dateFin) {
        this.nomTournoi = nomTournoi;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
    }

    public String getNomTournoi() {
        return nomTournoi;
    }

    public void setNomTournoi(String nomTournoi) {
        this.nomTournoi = nomTournoi;
    }

    public Calendar getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Calendar dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Calendar getDateFin() {
        return dateFin;
    }

    public void setDateFin(Calendar dateFin) {
        this.dateFin = dateFin;
    }
    
}
