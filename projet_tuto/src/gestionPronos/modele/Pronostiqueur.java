/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gestionPronos.modele;

/**
 *
 * @author Antoine
 */
public class Pronostiqueur {
    private int idPronostiqueur;
    private String nomPrenom;
    private String adresseMail;
    private String login;
    private String mdp;
    private boolean estAdmin;

    public Pronostiqueur(int idPronostiqueur, String nomPrenom, String adresseMail, String login, String mdp, boolean estAdmin) {
        this.idPronostiqueur = idPronostiqueur;
        this.nomPrenom = nomPrenom;
        this.adresseMail = adresseMail;
        this.login = login;
        this.mdp = mdp;
        this.estAdmin = estAdmin;
    }

    public Pronostiqueur(String nomPrenom, String adresseMail, String login, String mdp) {
        this.nomPrenom = nomPrenom;
        this.adresseMail = adresseMail;
        this.login = login;
        this.mdp = mdp;
    }

    public Pronostiqueur(int idPronostiqueur, String nomPrenom) {
        this.idPronostiqueur = idPronostiqueur;
        this.nomPrenom = nomPrenom;
    }
    
    
    
    public Pronostiqueur() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getIdPronostiqueur() {
        return idPronostiqueur;
    }



    
    
    
    
    // Getters et Setters
    public String getAdresseMail() {
        return adresseMail;
    }

    public void setAdresseMail(String adresseMail) {
        this.adresseMail = adresseMail;
    }

    public boolean isEstAdmin() {
        return estAdmin;
    }

    public void setEstAdmin(boolean estAdmin) {
        this.estAdmin = estAdmin;
    }
    
    public String getNomPrenom() {
        return nomPrenom;
    }

    public void setNomPrenom(String nomPrenom) {
        this.nomPrenom = nomPrenom;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }
    
}
