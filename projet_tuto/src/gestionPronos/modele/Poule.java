/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gestionPronos.modele;

import java.util.ArrayList;

/**
 *
 * @author Antoine
 */
public class Poule {
    private int idPoule;
    private String nomPoule;
    private int nbMaxEquipePoule;
    private ArrayList<Equipe> listeEquipe;

    public Poule(int idPoule, String nomPoule, int nbMaxEquipePoule, ArrayList<Equipe> listeEquipe) {
        this.idPoule = idPoule;
        this.nomPoule = nomPoule;
        this.nbMaxEquipePoule = nbMaxEquipePoule;
        this.listeEquipe = listeEquipe;
    }

    public Poule(int idPoule, String nomPoule, int nbMaxEquipePoule) {
        this.idPoule = idPoule;
        this.nomPoule = nomPoule;
        this.nbMaxEquipePoule = nbMaxEquipePoule;
    }

    public Poule(String nomPoule, ArrayList<Equipe> listeEquipe) {
        this.nomPoule = nomPoule;
        this.listeEquipe = listeEquipe;
    }
    
    
    
    @Override
    public String toString() {
        return this.nomPoule;
    }
    
    
    
    public boolean ajouterEquipe(Equipe equipe)
    {
        return this.listeEquipe.add(equipe);
    }
    
    public void setIdPoule(int idPoule) {
        this.idPoule = idPoule;
    }

    public void setNomPoule(String nomPoule) {
        this.nomPoule = nomPoule;
    }

    public void setNbMaxEquipePoule(int nbMaxEquipePoule) {
        this.nbMaxEquipePoule = nbMaxEquipePoule;
    }

    public void setListeEquipe(ArrayList<Equipe> listeEquipe) {
        this.listeEquipe = listeEquipe;
    }

    public int getIdPoule() {
        return idPoule;
    }

    public String getNomPoule() {
        return nomPoule;
    }

    public int getNbMaxEquipePoule() {
        return nbMaxEquipePoule;
    }

    public ArrayList<Equipe> getListeEquipe() {
        return listeEquipe;
    }
    
    
    
}
