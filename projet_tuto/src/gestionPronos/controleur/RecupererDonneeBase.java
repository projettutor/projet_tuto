/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gestionPronos.controleur;

import gestionPronos.modele.Equipe;
import gestionPronos.modele.Match;
import gestionPronos.modele.Pronostic;
import gestionPronos.modele.Pronostiqueur;
import gestionPronos.modele.Resultat;
import gestionPronos.modele.TypeResultatEquipeMatch;
import iutlr.dutinfo.bd.AccesBD;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
public class RecupererDonneeBase {
    private AccesBD acces;
    
    public RecupererDonneeBase()
    {
        this.acces = new AccesBD();
    }

    public Pronostiqueur recupererPronostiqueur(String login, String motDePasse)
    {
        try {
            
                List<List<String>> result = this.acces.interrogerBase("select idpro, nomprenom, adressemail, login, mdp, estAdmin from pronostiqueur where upper(login) = upper('" + login +"') and upper(mdp) = upper('" + motDePasse + "')");
                System.out.println("select idpro, nomprenom, login, mdp from pronostiqeur where upper(login) = upper('" + login +"') and mdp = '" + motDePasse + "'");
                if(!result.isEmpty())
                {
                    for(String s : result.get(0))
                    {
                        System.out.println(s);
                    }
                    return new Pronostiqueur(Integer.parseInt(result.get(0).get(0)),result.get(0).get(1), result.get(0).get(2), result.get(0).get(3), result.get(0).get(4), Boolean.parseBoolean(result.get(0).get(5)));
                }
                else
                    return null;
        } 
        catch (SQLException ex) {
            Logger.getLogger(RecupererDonneeBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public ArrayList<Resultat> recupererClassement()
    {
        try {
            List<List<String>> results = this.acces.interrogerBase("select pr.idpro, pr.nomprenom, r.nbPoints, r.classement from pronostiqueur pr, resultat r where pr.idpro = r.pronostiqueur");
            if(!results.isEmpty())
            {
                ArrayList<Resultat> listeRes = new ArrayList<>();
                for(List<String> lists : results)
                {
                    listeRes.add(new Resultat(Integer.parseInt(lists.get(3)), Integer.parseInt(lists.get(2)), new Pronostiqueur(Integer.parseInt(lists.get(0)), lists.get(1))));
                }
                return listeRes;
            }
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(RecupererDonneeBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
                
    }
    public ArrayList<Match> recupererMatchPronosticable(int idProno)
    {
        try {System.out.println("SELECT DISTINCT M.idMatch, PT.libelPhaseTournoi, EA.nomEquipe AS NomEquipeA, EA.pays AS PaysEquipeA, EB.nomEquipe AS NomEquipeB, EB.pays AS PaysEquipeB, M.dateDebut" +
                                                                   " FROM MATCH M, EQUIPE EA, EQUIPE EB, PHASE_TOURNOI PT, PRONOSTIC P, PRONOSTIQUEUR Pr" +
                                                                   " WHERE M.dateDebut>SYSDATE" +
                                                                   " AND M.phaseTournoi=PT.idPhaseTournoi" +
                                                                   " AND M.equipeB=EB.idEquipe" +
                                                                   " AND M.equipeA= EA.idEquipe" +
                                                                   " AND M.idMatch NOT IN (" +
                                                                   " SELECT P1.match FROM PRONOSTIC P1" +
                                                                   " WHERE P1.pronostiqueur="+ idProno +")"+
                                                                   " ORDER BY 1;");
            List<List<String>> results = this.acces.interrogerBase("SELECT DISTINCT M.idMatch, PT.libelPhaseTournoi, EA.nomEquipe AS NomEquipeA, EA.pays AS PaysEquipeA, EB.nomEquipe AS NomEquipeB, EB.pays AS PaysEquipeB, M.dateDebut" +
                                                                   " FROM MATCH M, EQUIPE EA, EQUIPE EB, PHASE_TOURNOI PT, PRONOSTIC P, PRONOSTIQUEUR Pr" +
                                                                   " WHERE M.dateDebut>SYSDATE" +
                                                                   " AND M.phaseTournoi=PT.idPhaseTournoi" +
                                                                   " AND M.equipeB=EB.idEquipe" +
                                                                   " AND M.equipeA= EA.idEquipe" +
                                                                   " AND M.idMatch NOT IN (" +
                                                                   " SELECT P1.match FROM PRONOSTIC P1" +
                                                                   " WHERE P1.pronostiqueur="+ idProno +")" +
                                                                   " ORDER BY 1");
            if(!results.isEmpty() )
            {
                ArrayList<Match> retour = new ArrayList<>();
                for(List<String> list : results)
                {
                    retour.add(new Match(Integer.parseInt(list.get(0)), list.get(6), new Equipe(list.get(2), list.get(3)), new Equipe(list.get(4), list.get(5))));
                }

                return retour;
            }
            else
                return null;
        } catch (SQLException ex) {
            Logger.getLogger(RecupererDonneeBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    /*public ArrayList<Match> recupererMatch()
    {
        System.out.println("SELECT EA.nomEquipe, EB.nomEquipe, M.idMatch\n" +
        "FROM MATCH M, EQUIPE EA, EQUIPE EB\n" +
        "WHERE M.equipeA=EA.idEquipe\n" +
        "AND M.equipeB=EB.idEquipe;");
    }*/
    
    public void declarerEquipes(ArrayList<Equipe> equipes)
    {
        for(Equipe e : equipes)
        {
            try {
                this.acces.mettreAjourBase("insert into equipe (nomequipe,pays) values ('" + e.getNomEquipe() + "', '" + e.getPays() + "')");
            } catch (SQLException ex) {
                Logger.getLogger(RecupererDonneeBase.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public ArrayList<Match> recupererMatch(){
        try {
            List<List<String>> results = this.acces.interrogerBase("select m.idmatch, m.datedebut, ea.nomequipe, ea.pays, eb.nomequipe, eb.pays from match m, equipe ea, equipe eb where m.equipea = ea.idequipe and m.equipeb = eb.idequipe ");
            
            if(!results.isEmpty())
            {
                ArrayList<Match> matchs = new ArrayList<>();
                for(List<String> lists : results)
                {
                    matchs.add(new Match(Integer.parseInt(lists.get(0)), lists.get(1), new Equipe(lists.get(2), lists.get(3)), new Equipe(lists.get(4), lists.get(5))));
                }
                return matchs;
            }
        } catch (SQLException ex) {
            Logger.getLogger(RecupererDonneeBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }   
    
        public ArrayList<Match> recupererMatchSansScore(){
        try {
            System.out.println("select m.idmatch, m.datedebut, ea.nomequipe, ea.pays, eb.nomequipe, eb.pays from match m, equipe ea, equipe eb where m.equipea = ea.idequipe and m.equipeb = eb.idequipe and m.resultateqa is not null ");
            List<List<String>> results = this.acces.interrogerBase("select m.idmatch, m.datedebut, ea.nomequipe, ea.pays, eb.nomequipe, eb.pays from match m, equipe ea, equipe eb where m.equipea = ea.idequipe and m.equipeb = eb.idequipe and m.scoreeqa = -1 ");
            
            if(!results.isEmpty())
            {
                ArrayList<Match> matchs = new ArrayList<>();
                for(List<String> lists : results)
                {
                    matchs.add(new Match(Integer.parseInt(lists.get(0)), lists.get(1), new Equipe(lists.get(2), lists.get(3)), new Equipe(lists.get(4), lists.get(5))));
                }
                return matchs;
            }
        } catch (SQLException ex) {
            Logger.getLogger(RecupererDonneeBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }   
    
    public boolean equipeExistante(Equipe equipe)
    {
        try {
            List<List<String>> result = this.acces.interrogerBase("select nomequipe, pays from equipe where nomequipe = '" + equipe.getNomEquipe() + "' and pays = '"+ equipe.getPays() +"'");
            
            if(!result.isEmpty())
            {
                Equipe equiResu = new Equipe(result.get(0).get(0), result.get(0).get(1));
                return equipe.equals(equiResu);
            }
            else
                return false;
        } catch (SQLException ex) {
            Logger.getLogger(RecupererDonneeBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public ArrayList<Equipe> recupEquipes()
    {
        try {
            List<List<String>> resultat = this.acces.interrogerBase("select nomequipe, pays from equipe");
            if(!resultat.isEmpty())
            {
                ArrayList<Equipe> equipes = new ArrayList<>();
                for(List<String> lS : resultat)
                {
                    equipes.add(new Equipe(lS.get(0), lS.get(1))); 
                }
                return equipes;
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(RecupererDonneeBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public ArrayList<Pronostic> recupProno(Pronostiqueur prono)
    {
        try {
                        System.out.println("SELECT Pc.datepronostic, lower(pc.resultatEQA), lower(pc.resultatEqB)" +
                    " ,m.idmatch, pt.libelphasetournoi, ea.nomequipe, ea.pays, eb.nomequipe, eb.pays, m.datedebut, m.scoreeqa, m.scoreeqb, m.datefin " +
                    "FROM MATCH M, EQUIPE EA, EQUIPE EB, PRONOSTIQUEUR Pr, PRONOSTIC Pc, phase_tournoi pt" +
                    " WHERE upper(Pr.nomPrenom)=upper('"+ prono.getNomPrenom() + "')" +
                    " AND Pr.idPro=Pc.pronostiqueur" +
                    " AND Pc.match=M.idMatch" +
                    " AND M.equipeB=EB.idEquipe" +
                    " AND M.equipeA= EA.idEquipe"
                    + " AND pt.idphasetournoi = m.phasetournoi" +
                    " ORDER BY Pc.datePronostic ASC");
            List<List<String>> results = this.acces.interrogerBase("SELECT Pc.datepronostic, lower(pc.resultatEQA), lower(pc.resultatEqB)" +
                    " ,m.idmatch, pt.libelphasetournoi, ea.nomequipe, ea.pays, eb.nomequipe, eb.pays, m.datedebut, m.scoreeqa, m.scoreeqb, m.datefin " +
                    "FROM MATCH M, EQUIPE EA, EQUIPE EB, PRONOSTIQUEUR Pr, PRONOSTIC Pc, phase_tournoi pt" +
                    " WHERE upper(Pr.nomPrenom)=upper('"+ prono.getNomPrenom() + "')" +
                    " AND Pr.idPro=Pc.pronostiqueur" +
                    " AND Pc.match=M.idMatch" +
                    " AND M.equipeB=EB.idEquipe" +
                    " AND M.equipeA= EA.idEquipe"
                    + " AND pt.idphasetournoi = m.phasetournoi" +
                    " ORDER BY Pc.datePronostic ASC");

            ArrayList<Pronostic> pronoRetour = new ArrayList<>();
            for(List<String> lists : results)
            {
                System.out.println(lists.get(1).charAt(0) + " " + lists.get(2).charAt(0));
                pronoRetour.add(new Pronostic(lists.get(1).charAt(0),lists.get(2).charAt(0),prono ,new Match(Integer.parseInt(lists.get(3)), lists.get(9), Integer.parseInt(lists.get(10)),Integer.parseInt(lists.get(11)),new Equipe(lists.get(5), lists.get(6)), new Equipe(lists.get(7), lists.get(8)), lists.get(12)), lists.get(0) ));
            }
            return pronoRetour;
        } catch (SQLException ex) {
            Logger.getLogger(RecupererDonneeBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public ArrayList<Match> recupeMatchPhase(int i){
        return new ArrayList();
    }
    
    public List<List<String>> recupeScore(){
         try {
               System.out.println("SELECT ROW_NUMBER() OVER (ORDER BY R.nbPoints DESC) AS Classement, P.idPro,  R.nbPoints FROM PRONOSTIQUEUR P, RESULTAT RWHERE P.idPro=R.pronostiqueur;");
               List<List<String>> results = this.acces.interrogerBase ("SELECT ROW_NUMBER() OVER (ORDER BY R.nbPoints DESC) AS Classement, P.idPro,  R.nbPoints FROM PRONOSTIQUEUR P, RESULTAT RWHERE P.idPro=R.pronostiqueur;");      
         return results;
         }
         catch (SQLException ex) {
            Logger.getLogger(RecupererDonneeBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }


}
