/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gestionPronos.controleur;

import gestionPronos.modele.Equipe;
import gestionPronos.modele.Match;
import gestionPronos.modele.Poule;
import gestionPronos.modele.Pronostic;
import gestionPronos.modele.Pronostiqueur;
import gestionPronos.modele.TypeResultatEquipeMatch;
import iutlr.dutinfo.bd.AccesBD;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Antoine
 */
public class EnregistrerDonneeBase {
    private AccesBD accesBd;
    
    public EnregistrerDonneeBase()
    {
        this.accesBd = new AccesBD();
    }
    
    public int declarerPronostiqueur(Pronostiqueur pronos)
    {
        try {
            return this.accesBd.mettreAjourBase("insert into pronostiqueur(nomPrenom, login, mdp, adresseMail)values('"
                    + pronos.getNomPrenom() +"','"
                    + pronos.getLogin() + "', '"
                    + pronos.getMdp() + "','"
                    + pronos.getAdresseMail() + "')");
        } catch (SQLException ex) {
            Logger.getLogger(EnregistrerDonneeBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
    
    public void declarerPoule(Poule pouleAAdd)
    {
        for(int i = 0; i < pouleAAdd.getListeEquipe().size(); i++)
        {
            try {
                System.out.println("insert into poule values (1, '" + pouleAAdd.getNomPoule() + "', 1, (select idequipe from equipe where nomEquipe = '" + pouleAAdd.getListeEquipe().get(i).getNomEquipe() + "' and pays='"+ pouleAAdd.getListeEquipe().get(i).getPays() +"'),4");
                this.accesBd.mettreAjourBase("insert into poule values (1, '" + pouleAAdd.getNomPoule() + "', 1, (select idequipe from equipe where nomEquipe = '" + pouleAAdd.getListeEquipe().get(i).getNomEquipe() + "' and pays='"+ pouleAAdd.getListeEquipe().get(i).getPays() +"'),4)");
                System.out.println("insert into poule values (1, '" + pouleAAdd.getNomPoule() + "', 1, (select idequipe from equipe where nomEquipe = '" + pouleAAdd.getListeEquipe().get(i).getNomEquipe() + "' and pays='"+ pouleAAdd.getListeEquipe().get(i).getPays() +"'),4");
            } catch (SQLException ex) {
                Logger.getLogger(EnregistrerDonneeBase.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public int pronostiquer(Pronostic prono)
    {
        try {
            System.out.println("insert into pronostic values(" + prono.getPronostiqueur().getIdPronostiqueur() + "," + prono.getMatch().getIdMatch() + ", sysdate,'" + prono.getResultatEqA().getCaractere()+ "','" + prono.getResultatEqB().getCaractere() +"')");
            return this.accesBd.mettreAjourBase("insert into pronostic values(" + prono.getPronostiqueur().getIdPronostiqueur() + "," + prono.getMatch().getIdMatch() + ", sysdate,'" + prono.getResultatEqA().getCaractere()+ "','" + prono.getResultatEqB().getCaractere() +"')");
        } catch (SQLException ex) {
            Logger.getLogger(EnregistrerDonneeBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
    
    public void declareEquipe (Equipe equipe){
        try {
            this.accesBd.mettreAjourBase("insert into EQUIPE (nomequipe, pays)values('"
                    + equipe.getNomEquipe()+"','"
                    + equipe.getPays()+ "')");
        } catch (SQLException ex) {
            Logger.getLogger(EnregistrerDonneeBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void creerClassement()
    {
        int nbPronos;
        try {
            
            nbPronos = Integer.parseInt(this.accesBd.interrogerBase("select count(*) from pronostiqueur").get(0).get(0));
            System.out.println(nbPronos);
            for(int i = 1; i < nbPronos+1;i++)
            {
                this.accesBd.mettreAjourBase("update resultat set nbpoints=(select count(*) from pronostiqueur pr, pronostic p, match m where pr.idpro = p.pronostiqueur and m.idmatch = p.match and m.resultateqa = p.resultateqa and idpro = "+i+") where pronostiqueur = " + i);
            }
            int[] tabId = new int[nbPronos];
            int[] tabClassement = new int[nbPronos];
            List<List<String>> results = this.accesBd.interrogerBase("SELECT RANK() OVER (ORDER BY R.nbPoints DESC) AS Classement, P.idPro FROM PRONOSTIQUEUR P, RESULTAT R WHERE P.idPro=r.pronostiqueur");
            for(int i = 0; i < nbPronos; i++)
            {
                if(!results.isEmpty())
                {
                    tabId[i] = Integer.parseInt(results.get(i).get(1));
                    tabClassement[i] = Integer.parseInt(results.get(i).get(0));
                }
            }
            for(int i =0 ; i < nbPronos ; i++)
            {
                this.accesBd.mettreAjourBase("UPDATE RESULTAT SET classement= " + tabClassement[i] + " WHERE pronostiqueur = " + tabId[i]);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EnregistrerDonneeBase.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void saisirResult(Match m, int scoreA, int scoreB) {
        TypeResultatEquipeMatch reseq1, reseq2;
        if(scoreA > scoreB)
        {
                reseq1 = TypeResultatEquipeMatch.VICTOIRE;
                reseq2 = TypeResultatEquipeMatch.DEFAITE;
        }
        else if(scoreA < scoreB)
        {
            reseq2 = TypeResultatEquipeMatch.VICTOIRE;
            reseq1 = TypeResultatEquipeMatch.DEFAITE;
        }
        else
        {
              reseq1 = TypeResultatEquipeMatch.NUL;  
              reseq2 = TypeResultatEquipeMatch.NUL;
        }
        try {
            this.accesBd.mettreAjourBase("update match set resultateqa = '" + reseq1.getCaractere() + "' , scoreeqa = "+ scoreA + ", scoreeqb="+ scoreB+ ",resultateqb = '" + reseq2.getCaractere() + "', datefin = sysdate where idmatch=" + m.getIdMatch() );
        } catch (SQLException ex) {
            Logger.getLogger(EnregistrerDonneeBase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    

}
