/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gestionPronos.controleur;

import gestionPronos.modele.Equipe;
import gestionPronos.modele.Match;
import gestionPronos.modele.PhaseTournoi;
import gestionPronos.modele.Poule;
import gestionPronos.modele.Pronostic;
import gestionPronos.modele.Pronostiqueur;
import gestionPronos.modele.Resultat;
import gestionPronos.vue.VueProno;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Antoine
 */
public class ControleurProno {
    private VueProno vue;
    private RecupererDonneeBase rdb;
    private EnregistrerDonneeBase eDB;
    private Pronostiqueur pronos;
    private SupressionDonneeBase sDB;
    private ArrayList<Poule> poules;
    
    public ControleurProno()
    {
        this.rdb = new RecupererDonneeBase();
        this.eDB = new EnregistrerDonneeBase();
        this.sDB = new SupressionDonneeBase();
        this.poules = new ArrayList<>();
    }
    
    
    /* Gestion des Vues ------------------------------------*/
    
    
    public void setVue(VueProno vue)
    {
        this.vue = vue;
    }
    
    public void retourMenuPrinc()
    {
        this.vue.menuPrincipal();
    }
    public void declaUser()
    {
        this.vue.declaUser();
    }
    
    public void ecranHisto()
    {
        this.vue.ecranHisto(this.rdb.recupProno(pronos));
    }
    
    public void retourMenuAdmin()
    {
        this.vue.menuAdmin();
    }
    public void panelDeclaEquipe() 
    {
        this.vue.panelDecalEquipe(this.recupererEquipeExistante());
    }
    public void panelDetailPronostiqueur()
    {
        this.vue.panelDetailsPrononstiqueur();
    }
    public void panelSaisirResultMatch()
    {
        this.vue.panelSaisirResulMatch();
    }
    
    
    
       
     /* Gestion Données BD --------------------------------------------*/ 
    
    
    public boolean verifierUtilisateur(String login, String motDePasse)
    {
        //TODO: test de l'identité
        this.pronos = rdb.recupererPronostiqueur(login, motDePasse);
        if(this.pronos != null)
        {
            this.vue.setInfosPronostiqueurConnecte(pronos);
            return true;
        }

        else
           return false;
    }
    
    public ArrayList<Match> recupMatchSansScore()
    {
        return this.rdb.recupererMatchSansScore();
    }
    
    public ArrayList<Resultat> recupClassement()
    {
        return this.rdb.recupererClassement();
    }
    public void creerClassement()
    {
        this.eDB.creerClassement();
    }
    public void creerPoule(int nbPoules)
    {
        for(int i = 0; i < nbPoules; i++)
        {
            this.poules.add(new Poule(i+1, "Poule" + ('A'+ i), 5));
        }
    }
    public boolean ajouterEquipeAPoule(Equipe equipe, int poule)
    {
        if(this.poules.get(poule).getListeEquipe().size() == 5)
            return false;
        else
           return this.poules.get(poule).ajouterEquipe(equipe);
    }
    public boolean pronostiquer(Pronostic prono)
    {
        return this.eDB.pronostiquer(prono) > 0;
    }
    public ArrayList<Match> recupMatchNonJouee()
    {
        return this.rdb.recupererMatchPronosticable(this.pronos.getIdPronostiqueur());
    }
    

    public ArrayList<Pronostic> recupPronostiqueJoueur(){
         return new ArrayList();
    }
   public ArrayList<Match> recupeListeMatch(PhaseTournoi phase){
      return this.rdb.recupeMatchPhase(phase.getIdPhaseTournoi());
     }   
   
   
   
    public Pronostiqueur getPronos() {
        return pronos;
    }
    
    public void creerUtilisateur(String nomPrenom, String login, String motDePasse, String adresseMail)
    {
        this.eDB.declarerPronostiqueur(new Pronostiqueur(nomPrenom, adresseMail, login, motDePasse));
    }
       
   public void ajouterEquipe(String nom,String pays){
       Equipe equipe = new Equipe(nom , pays);
       this.eDB.declareEquipe(equipe);
   }
   
   public ArrayList<Equipe> recupererEquipeExistante()
   {
       return this.rdb.recupEquipes();
   }
   
   public boolean equipeExiste(String nom, String pays){
       return this.rdb.equipeExistante(new Equipe(nom, pays));
   }
   public void supprimerEquipe(String nomEquipe, String pays)
   {
       this.sDB.supprimerEquipe(nomEquipe, pays);
   }
 
   public void ajouterPoule(Poule poule ){
       this.eDB.declarerPoule(poule);
       
   }
   
   public ArrayList<Match> recupMatch(){
       return this.rdb.recupererMatch();
   }
   
   public void saisirResult(Match m, int scoreA, int scoreB){
       this.eDB.saisirResult(m , scoreA , scoreB);
   }

    public ArrayList<Poule> getListPoules() {
        return null;
    }
       
    
}
