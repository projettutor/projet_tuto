/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gestionPronos.vue.fonctionnalitesAdmin;

import gestionPronos.controleur.ControleurProno;
import gestionPronos.exceptions.AdresseMailNonSaisieException;
import gestionPronos.exceptions.MdpUtilisateurNonSaisiException;
import gestionPronos.exceptions.NomPrenomNonSaisiException;
import gestionPronos.exceptions.NomUtilisateurNonSaisiException;
import gestionPronos.exceptions.UtilisateurExisteDejaException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Antoine
 */
public class DeclarerUser extends javax.swing.JPanel {
    private ControleurProno cprono;
    /**
     * Creates new form DeclarerUser
     */
    public DeclarerUser(ControleurProno cprono) {
        initComponents();
        this.cprono = cprono;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        mdp = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        mail = new javax.swing.JTextField();
        nomPrenom = new javax.swing.JTextField();
        login = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("Déclarer un utilisateur");

        jLabel2.setText("Nom et prénom du pronostiqueur");

        jLabel3.setText("Adresse e-mail du pronostiqueur");

        jLabel4.setText("Nom d'utilisateur du pronostiqueur");

        jLabel5.setText("Mot de passe du pronostiqueur");

        mdp.setPreferredSize(new java.awt.Dimension(60, 20));

        jButton1.setText("Sauvegarder");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        mail.setPreferredSize(new java.awt.Dimension(60, 20));

        nomPrenom.setPreferredSize(new java.awt.Dimension(60, 20));

        login.setPreferredSize(new java.awt.Dimension(60, 20));

        jButton2.setText("Quitter");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(69, 69, 69)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel2)
                                    .addComponent(nomPrenom, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(login, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel5)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(mail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                    .addComponent(mdp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                .addContainerGap(128, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(32, 32, 32)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(nomPrenom, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(mail, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(login, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(2, 2, 2)
                        .addComponent(mdp, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(58, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
       if (this.mail.getText().isEmpty()){
           try {
               throw new AdresseMailNonSaisieException("Erreur l'adresse email doit être saisi");
           } catch (AdresseMailNonSaisieException ex) {
               Logger.getLogger(DeclarerUser.class.getName()).log(Level.SEVERE, null, ex);
           }
       }
       else if (this.mdp.getText().isEmpty()){
           try {
               throw new MdpUtilisateurNonSaisiException("Erreur un mot de passe doit être saisi");
           } catch (MdpUtilisateurNonSaisiException ex) {
               Logger.getLogger(DeclarerUser.class.getName()).log(Level.SEVERE, null, ex);
           }
           
       }
       else if(nomPrenom.getText().isEmpty()){
           try {
               throw new  NomPrenomNonSaisiException("Erreur le nom et le prénom doivent être saisis");
           } catch (NomPrenomNonSaisiException ex) {
               Logger.getLogger(DeclarerUser.class.getName()).log(Level.SEVERE, null, ex);
           }
       }
           else if(this.login.getText().isEmpty()){
           try {
               throw new NomUtilisateurNonSaisiException();
           } catch (NomUtilisateurNonSaisiException ex) {
               Logger.getLogger(DeclarerUser.class.getName()).log(Level.SEVERE, null, ex);
           }
                   }
           else if(this.cprono.verifierUtilisateur(this.login.getText(), this.mdp.getText())) {
           try {
               throw new UtilisateurExisteDejaException("Ce login et ce mot de passe sont déjà utilisé");
           } catch (UtilisateurExisteDejaException ex) {
               Logger.getLogger(DeclarerUser.class.getName()).log(Level.SEVERE, null, ex);
           }
           }
             else  
                 this.cprono.creerUtilisateur(this.nomPrenom.getText(), this.login.getText(), this.mdp.getText(), this.mail.getText());
    }//GEN-LAST:event_jButton1ActionPerformed
public void effacerSelection(){
    this.mail.setText(null);
    this.mdp.setText(null);
    this.login.setText(null);
    this.nomPrenom.setText(null);
}
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        this.effacerSelection();
        this.cprono.retourMenuAdmin();
    }//GEN-LAST:event_jButton2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JTextField login;
    private javax.swing.JTextField mail;
    private javax.swing.JTextField mdp;
    private javax.swing.JTextField nomPrenom;
    // End of variables declaration//GEN-END:variables
}
