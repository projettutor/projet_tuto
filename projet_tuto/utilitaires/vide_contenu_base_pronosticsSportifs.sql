-- ============================================================================
-- Fichier : vide_contenu_base_pronosticsSportifs
-- Auteur  : Jamal Malki
-- Date    : Mai 2014
-- Role    : Creation des dépendances de références sur la base de donnees 
--			  "Pronostics-Sportifs" 
-- ============================================================================
delete from PRONOSTIC ;
delete from PRONOSTIQUEUR ;
delete from MATCH ;
delete from POULE ;
delete from EQUIPE ;
delete from TOURNOI ;
delete from PHASE_TOURNOI;
delete from CONFIG ;
delete from RESULTAT ;


-- Réinitialisation des séquences et triggers


DROP SEQUENCE incrementation_t1;
DROP SEQUENCE incrementation_t2;
DROP SEQUENCE incrementation_t3;

DROP TRIGGER trigger_t1;
DROP TRIGGER trigger_t2;
DROP TRIGGER trigger_t3;