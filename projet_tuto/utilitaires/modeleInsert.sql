INSERT INTO EQUIPE (nomEquipe,pays) VALUES ('France', 'France');
INSERT INTO EQUIPE (nomEquipe,pays) VALUES ('Bresil', 'Bresil');

INSERT INTO TOURNOI VALUES (1,'Coupe du Monde 2014', '12/06/2014', '13/07/2014', NULL);

INSERT INTO PHASE_TOURNOI VALUES (1,'POULE',1,NULL);

INSERT INTO POULE VALUES (1, 'A', 1,1,4);

INSERT INTO MATCH (dateDebut, equipeA, equipeB, dateFin, scoreEqA, scoreEqB, resultatEqA, resultatEqB, phaseTournoi)
VALUES ('13/06/2014',1,2,NULL,NULL,NULL,NULL,NULL, 1);

INSERT INTO PRONOSTIQUEUR (nomPrenom,login,mdp,estAdmin,adresseMail)
VALUE ('Jean LoL','jeanl','jeanl','FALSE','jeanlol@exemple.org');

INSERT INTO PRONOSTIC VALUES (1,1,'12/06/2014','G','P');

INSERT INTO RESULTAT VALUES (1,1,0,0);
