-- ============================================================================
-- Fichier : ddl_pronosticsSportifs
-- Auteur  : Jamal Malki
-- Date    : Mai 2014
-- Role    : Creation des tables de la base de donnees "Pronostics-Sportifs" 
--			 Les ordres SQL sont donnés par ordre alphabétique des noms
--			 des tables 
-- ============================================================================
 
-- ============================================================================
-- Creation de la Table CONFIG_TOURNOI                               
-- ============================================================================
 create table CONFIG (
idConfig			NUMBER						NOT NULL,
pronosticOK			NUMBER						NOT NULL,
pronosticKO			NUMBER						NOT NULL,
pasPronostic		NUMBER						NOT NULL,
constraint pk_CONFIG_TOURNOI primary key (idConfig)
);

-- ============================================================================
-- Creation de la Table EQUIPE                               
-- ============================================================================
 create table EQUIPE (
idEquipe		NUMBER						NOT NULL,
nomEquipe		VARCHAR2(20)				NOT NULL, 
pays			VARCHAR2(50)				NOT NULL,
constraint pk_EQUIPE primary key (idEquipe),
constraint uq_EQUIPE unique (nomEquipe)
);

-- ============================================================================
-- Creation de la Table MATCH                               
-- ============================================================================
 create table MATCH (
idMatch			NUMBER						NOT NULL,
dateDebut		DATE default sysdate		NOT NULL,
equipeA			NUMBER						NOT NULL,
equipeB			NUMBER						NOT NULL,
dateFin			DATE 						NULL,
scoreEqA		NUMBER						NULL,
scoreEqB		NUMBER						NULL,
resultatEqA		CHAR(1)						NULL,
resultatEqB		CHAR(1)						NULL,
phaseTournoi	NUMBER						NOT NULL,
constraint pk_MATCH primary key (idMatch),
constraint uq_MATCH unique (dateDebut, equipeA, equipeB),
constraint ck_match_resultatEqA check (upper(resultatEqA) in ('G', 'N', 'P')),
constraint ck_match_resultatEqB check (upper(resultatEqB) in ('G', 'N', 'P')),
constraint ck_match_equipeAG check (upper(resultatEqA) <> 'G' 
									OR  upper(resultatEqB) = 'P'),
constraint ck_match_equipeBG check (upper(resultatEqB) <> 'G' 
									OR  upper(resultatEqA) = 'P'),
constraint ck_match_equipeAP check (upper(resultatEqA) <> 'P' 
									OR  upper(resultatEqB) = 'G'),
constraint ck_match_equipeBP check (upper(resultatEqB) <> 'P' 
									OR  upper(resultatEqA) = 'G'),
constraint ck_match_equipeABN check (upper(resultatEqA) <> 'N' OR  upper(resultatEqB) = 'N')
);

-- ============================================================================
-- Creation de la Table PHASE_TOURNOI                               
-- ============================================================================
 create table PHASE_TOURNOI (
idPhaseTournoi					NUMBER						NOT NULL,
libelPhaseTournoi				VARCHAR2(20)				NOT NULL,
tournoi							NUMBER						NOT NULL,
config							NUMBER						NULL,
constraint pk_PHASE_TOURNOI primary key (idPhaseTournoi),
constraint ck_PhaseTournoi check(upper(libelPhaseTournoi) IN ('POULE', 
								'HUITIEME', 'QUART', 'DEMI', 'FINALE'))
);

-- ============================================================================
-- Creation de la Table POULE                               
-- ============================================================================
 create table POULE (
idPoule				NUMBER						NOT NULL,
tournoi				NUMBER						NOT NULL,
equipe				NUMBER						NOT NULL,
nbMaxEquipePoule	NUMBER						NOT NULL,
constraint pk_POULE primary key (idPoule, tournoi, equipe),
constraint uq_POULE unique (tournoi, equipe)
);

-- ============================================================================
-- Creation de la Table PRONOSTIC                               
-- ============================================================================
 create table PRONOSTIC (
pronostiqueur			NUMBER						NOT NULL,
match					NUMBER						NOT NULL,
datePronostic			DATE default sysdate		NOT NULL,
resultatEqA				CHAR(1)						NOT NULL,
resultatEqB				CHAR(1)						NOT NULL,
constraint pk_PRONOSTIC primary key (pronostiqueur, match),
constraint ck_pro_resultatEqA check (upper(resultatEqA) in ('G', 'N', 'P')),
constraint ck_pro_resultatEqB check (upper(resultatEqB) in ('G', 'N', 'P'))
);

-- ============================================================================
-- Creation de la Table PRONOSTIQUEUR                               
-- ============================================================================
 create table PRONOSTIQUEUR (
idPro			NUMBER						NOT NULL,
nomPrenom		VARCHAR2(100)				NOT NULL, 
login			VARCHAR2(20)				NOT NULL, 
mdp				VARCHAR2(20)				NOT NULL,
classement		NUMBER						NOT NULL,
estAdmin		NUMBER		default 0		NOT NULL,
constraint pk_PRONOSTIQUEUR primary key (idPro),
constraint uq_PRONOSTIQUEUR unique (login),
constraint kq_PRONOSTIQUEUR unique (classement),
constraint ck_PRONOSTIQUEUR check (estAdmin in (0,1))
);

-- ============================================================================
-- Creation de la Table RESULTAT                               
-- ============================================================================
 create table RESULTAT (
pronostiqueur	NUMBER						NOT NULL,
tournoi			NUMBER						NOT NULL,
nbPoints		NUMBER	default 0			NOT NULL,
classement		NUMBER	default 0			NOT NULL,
constraint pk_RESULTAT primary key (pronostiqueur, tournoi)
);

-- ============================================================================
-- Creation de la Table TOURNOI                               
-- ============================================================================
 create table TOURNOI (
idTournoi			NUMBER						NOT NULL,
nomTournoi			VARCHAR2(20)				NOT NULL, 
dateDebut			DATE						NOT NULL,
dateFin				DATE						NOT NULL,
config				NUMBER						NULL,
constraint pk_TOURNOI primary key (idTournoi),
constraint uq_TOURNOI unique (nomTournoi, dateDebut)
);






