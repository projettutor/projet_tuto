-- ============================================================================
-- Fichier : ddl_pronosticsSportifs
-- Auteur  : Jamal Malki
-- Date    : Mai 2014
-- Role    : Creation des dépendances de références sur la base de donnees 
--			  "Pronostics-Sportifs" 
-- ============================================================================

-- ============================================================================
-- DR de la Table TOURNOI                               
-- ============================================================================
alter table TOURNOI add
constraint fk_TOURNOI_CONFIG foreign key (config)
references CONFIG (idConfig);

-- ============================================================================
-- DR de la Table PHASE_TOURNOI                               
-- ============================================================================
alter table PHASE_TOURNOI add
constraint fk_PHASETOURNOI_TOURNOI foreign key (tournoi)
references TOURNOI (idTournoi);

alter table PHASE_TOURNOI add
constraint fk_PHASE_TOURNOI_CONFIG foreign key (config)
references CONFIG (idConfig);

-- ============================================================================
-- DR de la Table POULE                               
-- ============================================================================
alter table POULE add
constraint fk_POULE_TOURNOI foreign key (tournoi)
references TOURNOI (idTournoi);

alter table POULE add
constraint fk_POULE_EQUIPE foreign key (equipe)
references EQUIPE (idEquipe);

-- ============================================================================
-- DR de la Table MATCH                               
-- ============================================================================
alter table MATCH add
constraint fk_MATCH_EQUIPEA foreign key (equipeA)
references EQUIPE (idEquipe);

alter table MATCH add
constraint fk_MATCH_EQUIPEB foreign key (equipeB)
references EQUIPE (idEquipe);

alter table MATCH add
constraint fk_MATCH_PAHSETOURNOI foreign key (phaseTournoi)
references PHASE_TOURNOI (idPhaseTournoi);

-- ============================================================================
-- DR de la Table PRONOSTIC                               
-- ============================================================================
alter table PRONOSTIC add
constraint fk_PRONOSTIC_PRONOSTIQ foreign key (pronostiqueur)
references PRONOSTIQUEUR (idPro);

alter table PRONOSTIC add
constraint fk_PRONOSTIC_MATCH foreign key (match)
references MATCH (idMatch);

-- ============================================================================
-- DR de la Table RESULTAT                               
-- ============================================================================
alter table RESULTAT add
constraint fk_RESULTAT_PRONOSTQ foreign key (pronostiqueur)
references PRONOSTIQUEUR (idPro);

alter table RESULTAT add
constraint fk_RESULTAT_TOURNOI foreign key (tournoi)
references TOURNOI (idTournoi);



